<?php
/**
 * The template for displaying search results pages
*/
get_header();
?>
<!--contenido principal-->
<div id="main-content">
    <div class="container">
        <div id="content-area" class="clearfix row">
            <div id="left-area" class="col-md-7"> 
                <!--título general-->
                <h1>Resultados de su Búsqueda:</h1>
                <span class="et_pb_fullwidth_header_subhead"> Encontrar&aacute; m&aacute;s informaci&oacute;n en:</span>
                <!--buscador-->
                <div class="buscador center"><?php echo get_search_form(); ?></div>
                <?php if ( have_posts() ) : ?>
                <!--si hay post, entra en el bucle-->
                <?php while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <h1 class="entry-title main_title"><?php the_title(); ?></h1>
                    <div class="entry-content">
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink();?>">Leer más...</a>
                    </div>
                </article>
                <?php endwhile;?>
                <?php else: ?>
                <h2 class="text-center pt-4 pb-5">No hemos encontrado resultados para su b&uacute;squeda. Encontrar&aacute; un &iacute;ndice con todas
                las entradas de este blog en:</h2>
                <?php endif;?>
            </div>
            <?php if ( is_active_sidebar( 'sidebar-widget' ) ) : ?>
            <div id="sidebar-widget" class="primary-sidebar widget-area" role="complementary">
                <?php dynamic_sidebar( 'sidebar-widget' ); ?>
            </div>
            <?php endif; ?>
        </div> 
    </div> 
</div> 

<?php get_footer(); ?>